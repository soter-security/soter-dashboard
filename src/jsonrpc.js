import { v4 as uuidv4 } from 'uuid';
import { Promise } from 'q';


export class JsonRpcError extends Error {
    constructor(error) {
        super();
        this.code = error.code;
        this.message = error.message;
        this.data = error.data;
    }
}


export class ConnectionError extends Error {
    constructor(title, message) {
        super();
        this.title = title;
        this.message = message;
    }
}


class ConnectionClosed extends ConnectionError {
    constructor(event) {
        super(
            'Connection closed',
            event.reason || 'Server closed connection unexpectedly.'
        );
    }
}


class ConnectionFailed extends ConnectionError {
    constructor() {
        super('Connection error', 'Connection could not be established.');
    }
}


export const mstMiddleware = (onRpcError, onConnectionError) => (call, next) => {
    // If a flow is going to throw a JSON-RPC error, report it before proceeding
    if( call.type === "flow_resume_error" ) {
        const error = call.args[0];
        if( error instanceof JsonRpcError ) onRpcError(error);
        if( error instanceof ConnectionError ) onConnectionError(error);
    }
    return next(call);
};


// This function returns a proxy object that looks a bit like a websocket
// However there are some key differences:
//   1. If the websocket disconnects, it will be reconnected the next time it is used
//   2. The send method returns a promise that only resolves once the send has occurred
//      This allows for send to be called on a socket that is still opening
const makeSocket = (endpoint) => {
    // This is the actual current socket
    let currentSocket = new WebSocket(endpoint);
    // This is the event listeners that have been registered, so they can be replayed
    const eventListeners = { open: [], message: [], error: [], close: [] };

    // Function to send data in a way that is robust to the socket opening
    const sendData = (data, resolve, reject, attempt, maxAttempts) => {
        if( currentSocket.readyState === WebSocket.OPEN ) {
            // If the websocket is open, send the data
            currentSocket.send(data);
            resolve();
            return;
        }
        else if( [WebSocket.CONNECTING, WebSocket.CLOSING].includes(currentSocket.readyState) ) {
            // If the socket is in a transitional state, wait without incrementing the attempts
            setTimeout(() => sendData(data, resolve, reject, attempt, maxAttempts), 200 * attempt);
            return;
        }
        else {
            // If the socket is closed, decide whether to wait and try again or bail
            if( attempt <= maxAttempts ) {
                setTimeout(
                    () => sendData(data, resolve, reject, attempt + 1, maxAttempts),
                    // Extend the waiting time with each attempt
                    200 * attempt
                );
            }
            else {
                reject(new ConnectionFailed());
            }
        }
    };

    return ({
        addEventListener: (event, handler) => {
            eventListeners[event].push(handler);
            // Also attach the listener to the current socket
            if( currentSocket ) currentSocket.addEventListener(event, handler);
        },
        // Sending data is now a promise, so that error reporting can be deferred
        send: (data, maxAttempts = 3) => new Promise((resolve, reject) => {
            // If the websocket is closed, open a new one
            if( currentSocket.readyState === WebSocket.CLOSED ) {
                // Remove the event handlers from the old socket
                Object.entries(eventListeners).forEach(([event, handlers]) => {
                    handlers.forEach(handler => currentSocket.removeEventListener(event, handler));
                });
                // Create the new socket
                currentSocket = new WebSocket(endpoint);
                // Re-attach the event handlers
                Object.entries(eventListeners).forEach(([event, handlers]) => {
                    handlers.forEach(handler => currentSocket.addEventListener(event, handler));
                });
            }
            // Then attempt to send the data maxAttempts times, to give time for the socket to open
            sendData(data, resolve, reject, 1, maxAttempts)
        })
    });
}


export default {
    create: (endpoint) => {
        // Create an object to hold pending requests and a function to resolve them
        const pendingRequests = {};
        const resolvePendingRequest = (id, resultOrError) => {
            const [resolve, reject] = pendingRequests[id];
            delete pendingRequests[id];
            if( resultOrError instanceof Error ) {
                reject(resultOrError);
            }
            else {
                resolve(resultOrError);
            }
        };
        // Create the socket
        const socket = makeSocket(endpoint);
        // Listen for messages on the socket and resolve the corresponding request
        socket.addEventListener('message', (event) => {
            const response = JSON.parse(event.data);
            resolvePendingRequest(
                response.id,
                response.result !== undefined ?
                    response.result :
                    new JsonRpcError(response.error)
            );
        });
        // When the socket is closed, reject all pending requests
        // Note that close is a recurring event as underlying connections are opened and closed
        // Even if a new connection is opened for the next request, we still need to reject
        // the requests that depended on the old connection
        socket.addEventListener('close', (event) => {
            const error = new ConnectionClosed(event);
            // Resolve all the pending requests with a connection closed error
            Object.getOwnPropertyNames(pendingRequests).forEach(id => resolvePendingRequest(id, error));
        });
        // Return a function that can be called to execute a method
        return (method, params) => {
            const request = { jsonrpc: "2.0", method, params: params || {}, id: uuidv4() };
            // Return a promise that is resolved when the response is received
            const promise = new Promise((resolve, reject) => pendingRequests[request.id] = [resolve, reject]);
            // If the send fails, resolve the response with the error
            socket
                .send(JSON.stringify(request))
                .catch(error => resolvePendingRequest(request.id, error));
            // We return a promise that is resolved when the response is received
            return promise;
        };
    }
};
