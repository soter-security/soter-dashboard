import React from 'react';

import { useLocation } from 'react-router-dom';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { faCog, faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

import { observer } from 'mobx-react';

import queryString from 'query-string';

import IssuesTable, { IssueDetail, IssueDetailItem } from './IssuesTable';


const ImageScanForm = observer(({ disabled, initialImage, executeScan }) => {
    const [image, setImage] = React.useState(initialImage || '');
    const onSubmit = (e) => {
        e.preventDefault();
        e.stopPropagation();
        executeScan(image);
    };
    return (
        <Row className="mb-3">
            <Col>
                <form onSubmit={onSubmit}>
                    <InputGroup>
                        <Form.Control
                            id="image"
                            placeholder="Image"
                            aria-label="Image"
                            value={image}
                            onChange={(e) => setImage(e.target.value)}
                            disabled={disabled}
                            required
                        />
                        <InputGroup.Append>
                            <Button type="submit" disabled={disabled}>Scan</Button>
                        </InputGroup.Append>
                    </InputGroup>
                </form>
            </Col>
        </Row>
    );
});


const ImageIssuesTable = observer(({ scanResult }) => {
    return (
        <IssuesTable
            issues={scanResult.issues}
            captionText={
                <>
                    <p className="mb-2">{`Found ${scanResult.issues.length} issues for ${scanResult.image}`}</p>
                    <p className="mb-0">Digest: {scanResult.digest}</p>
                </>
            }
            extraColumns={[
                {
                    title: 'Package',
                    render: issue => issue.package_name || '-'
                },
                {
                    title: 'Version',
                    render: issue => issue.package_version || '-'
                },
                {
                    title: 'Type',
                    render: issue => issue.package_type || '-'
                },
                {
                    title: 'Fixed In',
                    render: issue => issue.fix_version || '-'
                }
            ]}
            detailRenderers={{
                ImageVulnerability: ({ issue }) => (
                    <IssueDetail issue={issue}>
                        <IssueDetailItem title="URL">
                            {issue.info_url ?
                                <a
                                    href={issue.info_url}
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    {issue.info_url}
                                    <Icon icon={faExternalLinkAlt} className="ml-2" />
                                </a> :
                                '-'
                            }
                        </IssueDetailItem>
                        <IssueDetailItem title="Package">{issue.package_name}</IssueDetailItem>
                        <IssueDetailItem title="Version">{issue.package_version}</IssueDetailItem>
                        <IssueDetailItem title="Type">{issue.package_type}</IssueDetailItem>
                        <IssueDetailItem title="Location">
                            {issue.package_location ?
                                <code>{issue.package_location}</code> :
                                '-'
                            }
                        </IssueDetailItem>
                        <IssueDetailItem title="Fixed In">{issue.fix_version || '-'}</IssueDetailItem>
                    </IssueDetail>
                ),
                Error: ({ issue }) => (
                    <IssueDetail issue={issue}>
                        <IssueDetailItem title="Error detail">{issue.detail}</IssueDetailItem>
                    </IssueDetail>
                )
            }}
        />
    );
});


const ImageScan = observer(({ imageScan }) => {
    const location = useLocation();
    // Get the image from the state or the query string
    const queryImage = location.state?.image || queryString.parse(location.search).image;
    // If an image is given as a query param, start a scan for it
    // We only need to do this once when the component is mounted
    // eslint-disable-next-line react-hooks/exhaustive-deps
    React.useEffect(() => { if( queryImage ) imageScan.executeScan(queryImage); }, []);
    return (
        <>
            <Row><Col><h1 className="border-bottom pb-2 mb-4">Scan an image</h1></Col></Row>
            <ImageScanForm
                disabled={imageScan.scanInProgress}
                // The first rendering is before the state gets updated, so we have to do this
                initialImage={queryImage || imageScan.scanResult?.image}
                executeScan={imageScan.executeScan}
            />
            <Row>
                <Col>
                    {imageScan.scanInProgress ? (
                        <div className="d-flex justify-content-center align-items-center py-5">
                            <Icon icon={faCog} size="2x" spin />
                            <div className="d-inline-block ml-2">Scanning...</div>
                        </div>
                    ) : (
                        imageScan.scanResult ? (
                            <ImageIssuesTable scanResult={imageScan.scanResult} />
                        ) : (
                            <div className="d-flex justify-content-center align-items-center py-5">
                                <div className="d-inline-block ml-2">No scan available.</div>
                            </div>
                        )
                    )}
                </Col>
            </Row>
        </>
    );
});


export default ImageScan;
