import React from 'react';

import { Link } from 'react-router-dom';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import InputGroup from 'react-bootstrap/InputGroup';
import Nav from 'react-bootstrap/Nav';

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import { faCog, faPlus, faRandom } from '@fortawesome/free-solid-svg-icons';

import { entries } from 'mobx';
import { observer } from 'mobx-react';

import RjsfForm from '@rjsf/bootstrap-4';

import ReactMarkdown from 'react-markdown';

import IssuesTable, { IssueDetail, IssueDetailItem } from './IssuesTable';


const ObjectFieldTemplate = ({ properties }) => properties.map((property, index) => (
    <React.Fragment key={index}>{property.content}</React.Fragment>
));


const FieldTemplate = ({ children }) => children;


const SchemaField = ({ schema: { ui: uiSchema = {}, ...schema }, ...props }) => {
    // Render a form without a form tag and no submit button
    return (
        <RjsfForm
            {...props}
            schema={schema}
            // We only want to modify the rendering of the top-level field, not the
            // entire form, so specify templates using the UI schema
            uiSchema={{
                ...uiSchema,
                "ui:ObjectFieldTemplate": ObjectFieldTemplate,
                "ui:FieldTemplate": FieldTemplate
            }}
            tagName="div"
        >
            <></>
        </RjsfForm>
    );
};


const ProfileModalButton = observer(({ namespaceScan }) => {
    const [showModal, setShowModal] = React.useState(false);
    // Maintain local state for the selected profile (different from the active profile)
    const [selectedProfileId, setSelectedProfileId] = React.useState(undefined);
    // Also maintain local state for the form
    const [profileData, setProfileData] = React.useState({ name: '', kind: '', params: {} });
    // When switching profiles, reset the form data
    const switchProfile = (toProfile = 'new') => {
        if( toProfile === 'new' ) {
            setSelectedProfileId(undefined);
            setProfileData({ name: '', kind: '', params: {} });
        }
        else {
            const profile = namespaceScan.getProfileById(toProfile);
            setSelectedProfileId(profile.id);
            setProfileData({ name: profile.name, kind: profile.kind, params: profile.params });
        }
    };
    // When the modal is opened, switch to the active profile
    const openModal = () => {
        setShowModal(true);
        switchProfile(namespaceScan.activeProfile?.id);
    };
    const closeModal = () => setShowModal(false);
    const handleNameChange = (event) => {
        const name = event.target.value;
        setProfileData(prev => ({ ...prev, name }));
    };
    const handleKindChange = (event) => {
        const kind = event.target.value;
        // Setting the kind should also reset the params
        setProfileData(prev => ({ ...prev, kind, params: {} }));
    };
    const handleParamsChange = (event) => {
        const params = event.formData;
        setProfileData(prev => ({ ...prev, params }));
    };
    // On submit, add or update the profile
    const handleSubmit = (event) => {
        event.preventDefault();
        event.stopPropagation();
        const profile = selectedProfileId ?
            namespaceScan.updateProfile(selectedProfileId, profileData) :
            namespaceScan.addProfile(profileData);
            namespaceScan.setActiveProfile(profile);
        closeModal();
    };
    // On delete, remove the currently selected id
    const handleDelete = () => {
        if( selectedProfileId ) {
            namespaceScan.removeProfile(selectedProfileId);
            // Jump back to the active profile when the selected one is deleted
            switchProfile(namespaceScan.activeProfile?.id);
        }
    };
    return (
        <>
            <Button onClick={openModal}>
                <span className="sr-only">Switch profile</span>
                <Icon icon={faRandom} />
            </Button>
            <Modal
                show={showModal}
                onHide={closeModal}
                restoreFocus={false}
                size="lg"
                className="profile-modal"
            >
                <Modal.Header>
                    <Modal.Title>Manage profiles</Modal.Title>
                </Modal.Header>
                <form onSubmit={handleSubmit}>
                    <Modal.Body>
                        <Row>
                            <Col lg={3}>
                                <Nav
                                    variant="pills"
                                    className="flex-column"
                                    activeKey={selectedProfileId || 'new'}
                                    defaultActiveKey="new"
                                    onSelect={switchProfile}
                                >
                                    <Nav.Item>
                                        {namespaceScan.profiles.map(profile => (
                                            <Nav.Link key={profile.id} eventKey={profile.id}>
                                                {profile.name}
                                            </Nav.Link>
                                        ))}
                                        <Nav.Link eventKey="new">
                                            <Icon icon={faPlus} fixedWidth className="mr-2" />
                                            New profile
                                        </Nav.Link>
                                    </Nav.Item>
                                </Nav>
                            </Col>
                            <Col lg={9}>
                                <Form.Group controlId="profileName">
                                    <Form.Label>Profile name</Form.Label>
                                    <Form.Control
                                        required
                                        placeholder="Profile name"
                                        value={profileData.name}
                                        onChange={handleNameChange}
                                    />
                                </Form.Group>
                                <Form.Group controlId="profileKind">
                                    <Form.Label>Profile kind</Form.Label>
                                    <Form.Control
                                        as="select"
                                        required
                                        value={profileData.kind}
                                        onChange={handleKindChange}
                                    >
                                        <option value="">Select a kind...</option>
                                        {entries(namespaceScan.authKinds).map(([kind, schema]) => (
                                            <option key={kind} value={kind}>{schema.title}</option>
                                        ))}
                                    </Form.Control>
                                </Form.Group>
                                {profileData.kind && (
                                    <SchemaField
                                        schema={namespaceScan.authKinds.get(profileData.kind)}
                                        formData={profileData.params}
                                        onChange={handleParamsChange}
                                    />
                                )}
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" disabled={!selectedProfileId} onClick={handleDelete}>
                            Delete
                        </Button>
                        <Button variant="outline-secondary" onClick={closeModal}>Cancel</Button>
                        <Button type="submit">Save</Button>
                    </Modal.Footer>
                </form>
            </Modal>
        </>
    );
});


const NamespaceScanForm = observer(({ namespaceScan }) => {
    const onSubmit = (event) => {
        event.preventDefault();
        event.stopPropagation();
        namespaceScan.executeScan();
    };
    return (
        <Row className="mb-3">
            <Col>
                <Form inline onSubmit={onSubmit} className="namespace-scan-form">
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="profile-label">Profile</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control
                            id="profile"
                            aria-label="Profile"
                            aria-describedby="profile-label"
                            value={namespaceScan.activeProfile?.name || 'No profile selected'}
                            readOnly
                        >
                        </Form.Control>
                        <InputGroup.Append>
                            <ProfileModalButton namespaceScan={namespaceScan} />
                        </InputGroup.Append>
                    </InputGroup>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="cluster-label">Cluster</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control
                            as="select"
                            id="cluster"
                            aria-label="Cluster"
                            aria-describedby="cluster-label"
                            value={namespaceScan.activeCluster}
                            onChange={(e) => namespaceScan.setActiveCluster(e.target.value)}
                            disabled={
                                namespaceScan.scanInProgress ||
                                !namespaceScan.clusters ||
                                namespaceScan.clusters.length < 2
                            }
                            required
                        >
                            <option value="">{
                                namespaceScan.activeProfile === undefined ? (
                                    'No profile selected'
                                ) : (
                                    namespaceScan.clusters === undefined ? (
                                        'Loading clusters...'
                                    ) : (
                                        namespaceScan.clusters.length > 0 ?
                                            'Select a cluster...' :
                                            'No available clusters'
                                    )
                                )
                            }</option>
                            {(namespaceScan.clusters || []).map(opt => <option key={opt}>{opt}</option>)}
                        </Form.Control>
                    </InputGroup>
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="namespace-label">Namespace</InputGroup.Text>
                        </InputGroup.Prepend>
                        <Form.Control
                            as="select"
                            id="namespace"
                            aria-label="Namespace"
                            aria-describedby="namespace-label"
                            value={namespaceScan.activeNamespace}
                            onChange={(e) => namespaceScan.setActiveNamespace(e.target.value)}
                            disabled={
                                namespaceScan.scanInProgress ||
                                !namespaceScan.namespaces ||
                                namespaceScan.namespaces.length < 2
                            }
                            required
                        >
                            <option value="">{
                                namespaceScan.activeCluster ? (
                                    namespaceScan.namespaces === undefined ? (
                                        'Loading namespaces...'
                                    ) : (
                                        namespaceScan.namespaces.length > 0 ?
                                            'Select a namespace...' :
                                            'No available namespaces'
                                    )
                                ) : (
                                    'No cluster selected'
                                )
                            }</option>
                            {(namespaceScan.namespaces || []).map(opt => <option key={opt}>{opt}</option>)}
                        </Form.Control>
                    </InputGroup>
                    <Button type="submit" disabled={namespaceScan.scanInProgress || !namespaceScan.activeNamespace}>
                        Scan
                    </Button>
                </Form>
            </Col>
        </Row>
    );
});


const AffectedResourcesItem = ({ issue }) => (
    <IssueDetailItem title="Affected Resources">
        <ul className="list-pills">
            {issue.affected_resources.map(resource => {
                const resourceName = `${resource.kind}/${resource.name}`;
                return <li key={resourceName}><code>{resourceName}</code></li>
            })}
        </ul>
    </IssueDetailItem>
);


const NamespaceIssuesTable = observer(({ scanResult }) => {
    const numResources = scanResult.resources.length;
    return (
        <Row>
            <Col>
                <IssuesTable
                    issues={scanResult.issues}
                    captionText={
                        `Found ${scanResult.issues.length} issues for ${numResources} ` +
                        `resource${numResources === 1 ? '': 's'}`
                    }
                    extraColumns={[
                        {
                            title: 'Affects',
                            render: (issue) => {
                                const numResources = issue.affected_resources.length;
                                return `${numResources} resource${numResources > 1 ? 's' : ''}`;
                            }
                        }
                    ]}
                    detailRenderers={{
                        VulnerableImage: ({ issue }) => {
                            return (
                                <IssueDetail issue={issue}>
                                    <IssueDetailItem title="Image">
                                        <Link
                                            to={{
                                                pathname: "/scans/image",
                                                state: { image: issue.image }
                                            }}
                                        >
                                            <code>{issue.image}</code>
                                        </Link>
                                    </IssueDetailItem>
                                    <AffectedResourcesItem issue={issue} />
                                </IssueDetail>
                            );
                        },
                        ConfigurationIssue: ({ issue }) => {
                            return (
                                <IssueDetail issue={issue}>
                                    <IssueDetailItem title="Suggested Remediation">
                                        <ReactMarkdown source={issue.suggested_remediation || '-'} />
                                    </IssueDetailItem>
                                    <AffectedResourcesItem issue={issue} />
                                </IssueDetail>
                            );
                        },
                        ResourceError: ({ issue }) => {
                            return (
                                <IssueDetail issue={issue}>
                                    <IssueDetailItem title="Error detail">{issue.detail}</IssueDetailItem>
                                    <AffectedResourcesItem issue={issue} />
                                </IssueDetail>
                            );
                        }
                    }}
                />
            </Col>
        </Row>
    );
});


const NamespaceScan = observer(({ namespaceScan }) => {
    return (
        <>
            <Row><Col><h1 className="border-bottom pb-2 mb-4">Scan Kubernetes resources</h1></Col></Row>
            <NamespaceScanForm namespaceScan={namespaceScan} />
            <Row>
                <Col>
                    {namespaceScan.scanInProgress ? (
                        <div className="d-flex justify-content-center align-items-center py-5">
                            <Icon icon={faCog} size="2x" spin />
                            <div className="d-inline-block ml-2">Scanning...</div>
                        </div>
                    ) : (
                        namespaceScan.scanResult ? (
                            <NamespaceIssuesTable scanResult={namespaceScan.scanResult} />
                        ) : (
                            <div className="d-flex justify-content-center align-items-center py-5">
                                <div className="d-inline-block ml-2">No scan available.</div>
                            </div>
                        )
                    )}
                </Col>
            </Row>
        </>
    );
});


export default NamespaceScan;
