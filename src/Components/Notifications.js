import React from 'react';
import ReactDOM from 'react-dom';

import Toast from 'react-bootstrap/Toast';

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import {
    faCheckCircle,
    faExclamationCircle,
    faExclamationTriangle,
    faInfoCircle
} from '@fortawesome/free-solid-svg-icons';

import { values } from 'mobx';
import { observer } from 'mobx-react';


const Icons = {
    info: faInfoCircle,
    success: faCheckCircle,
    warning: faExclamationTriangle,
    danger: faExclamationCircle
};


const Notification = observer(({ notification }) => {
    const icon = Icons[notification.level];
    const [visible, setVisible] = React.useState(true);
    const closeAndDismiss = () => {
        // Hide the notification to trigger the fade
        setVisible(false);
        // Then remove the notification from the array after the transition is done
        setTimeout(notification.dismiss, 1000);
    };
    return (
        <Toast
            className="notification"
            animation
            show={visible}
            onClose={closeAndDismiss}
            autohide={!!notification.duration}
            delay={notification.duration}
        >
            <Toast.Header className={`text-${notification.level} align-baseline`}>
                <Icon icon={icon} size="lg" className="mr-2" />
                <strong className="mr-auto">{notification.title}</strong>
            </Toast.Header>
            <Toast.Body>{notification.message}</Toast.Body>
        </Toast>
    )
});


const Notifications = observer(({ store }) => {
    // Render using a portal so that we can sit over other elements
    const [container] = React.useState(() => {
        const element = document.createElement('div');
        element.classList.add('notification-container');
        return element;
    });
    React.useEffect(() => {
        document.body.appendChild(container);
        return () => document.body.removeChild(container);
    });
    return ReactDOM.createPortal(
        <>
            {values(store.notifications).map(notification => (
                <Notification key={notification.id} notification={notification} />
            ))}
        </>,
        container
    );
});


export default Notifications;
