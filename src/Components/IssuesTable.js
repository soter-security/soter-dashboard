import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import {
    faExclamationCircle,
    faExclamationTriangle,
    faExternalLinkAlt,
    faInfo,
    faThumbsUp
} from '@fortawesome/free-solid-svg-icons';


export const IssueDetailItem = ({ title, children }) => (
    <Row className="issue-detail-item">
        <Col xs={12} lg={3}><strong>{title}</strong></Col>
        <Col xs={12} lg={9}>{children}</Col>
    </Row>
);


export const IssueDetail = ({ issue, children }) => {
    return (
        <>
            <IssueDetailItem title="Kind">
                <code>{issue.kind}</code>
            </IssueDetailItem>
            <IssueDetailItem title="Title">{issue.title}</IssueDetailItem>
            <IssueDetailItem title="Severity">
                <span className={`severity-text-${issue.severity.toLowerCase()}`}>
                    {issue.severity.toUpperCase()}
                </span>
            </IssueDetailItem>
            {children}
            <IssueDetailItem title="Reported By">
                <ul className="list-pills">
                    {issue.reported_by.map((scanner, i) => (
                        <li key={scanner}><code>{scanner}</code></li>
                    ))}
                </ul>
            </IssueDetailItem>
        </>
    );
};


const DefaultDetailRenderer = ({ issue }) => {
    // Extract anything that is not part of the base issue and render as JSON
    const { kind, title, severity, reported_by, ...additionalFields } = issue;
    return (
        <IssueDetail issue={issue}>
            {Object.entries(additionalFields).map(([name, value]) => (
                <IssueDetailItem key={name} title={name}>
                    <pre>{JSON.stringify(value, null, 2)}</pre>
                </IssueDetailItem>
            ))}
        </IssueDetail>
    );
};


const IssueDetailModal = ({ issue, detailRenderers, show, onHide }) => {
    // Work out the detail renderer to use based on the issue kind
    const DetailRenderer = detailRenderers[issue.kind] || DefaultDetailRenderer;
    return (
        <Modal
            size="lg"
            className={`issue-modal severity-${issue.severity.toLowerCase()}`}
            show={show}
            onHide={onHide}
        >
            <Modal.Header closeButton>
                <Modal.Title>{issue.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <DetailRenderer issue={issue} />
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
};


const SeverityIcon = {
    CRITICAL: faExclamationCircle,
    UNKNOWN: faExclamationCircle,
    HIGH: faExclamationCircle,
    MEDIUM: faExclamationTriangle,
    LOW: faInfo
};


const Issue = ({ issue, extraColumns, detailRenderers }) => {
    // In order for the collapse effect to render correctly, we need to apply it
    // to a div inside the slideout row
    // However we still need to hide the row
    const [modalVisible, setModalVisible] = React.useState(false);
    const showModal = () => setModalVisible(true);
    const hideModal = () => setModalVisible(false);
    // Work out the icon to use based on severity
    const icon = SeverityIcon[issue.severity];
    return (
        <>
            <tr
                className={`issue severity-${issue.severity.toLowerCase()} cursor-pointer`}
                onClick={showModal}
            >
                <td className="icon">{icon && <Icon icon={icon} fixedWidth />}</td>
                <td><code>{issue.kind}</code></td>
                <td>
                    {issue.info_url ?
                        <a
                            href={issue.info_url}
                            // We need to stop the event bubbling, otherwise clicking this link
                            // will cause the issue details to pop up
                            onClick={(e) => { e.stopPropagation(); return true; }}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            {issue.title}
                            <Icon icon={faExternalLinkAlt} className="ml-2" />
                        </a> :
                        issue.title
                    }
                </td>
                <td className="severity">{issue.severity.toUpperCase()}</td>
                {extraColumns.map((col, idx) => <td key={idx}>{col.render(issue)}</td>)}
            </tr>
            <IssueDetailModal
                issue={issue}
                detailRenderers={detailRenderers}
                show={modalVisible}
                onHide={hideModal}
            />
        </>
    );
};


const IssuesTable = ({ issues, captionText, extraColumns = [], detailRenderers = {} }) => {
    return (
        <Table className="issues-table" responsive bordered size="md">
            <thead>
                <tr>
                    <th></th>
                    <th>Kind</th>
                    <th>Title</th>
                    <th>Severity</th>
                    {/* Put any extra columns here */}
                    {extraColumns.map((col, idx) => <th key={idx}>{col.title}</th>)}
                </tr>
            </thead>
            <tbody>
                {issues.length > 0 ?
                    issues.map((issue, idx) => (
                        <Issue
                            key={idx}
                            issue={issue}
                            extraColumns={extraColumns}
                            detailRenderers={detailRenderers}
                        />
                    )) : (
                        <tr className="issue severity-success">
                            <td className="icon"><Icon icon={faThumbsUp} fixedWidth /></td>
                            <td colSpan={extraColumns.length + 3}>No issues found.</td>
                        </tr>
                    )
                }
            </tbody>
            <caption className="caption-top px-3 py-2">{captionText}</caption>
        </Table>
    );
};


export default IssuesTable;
