import React from 'react';
import {
    useHistory,
    useLocation
  } from "react-router-dom";

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import { observer } from 'mobx-react';


const Auth = observer(({ auth, updateAuth }) => {
    const history = useHistory(), location = useLocation();
    const { from } = location.state || { from: { pathname: "/" } };
    const [kubeconfig, setKubeconfig] = React.useState(auth ? auth.params.kubeconfig : '');
    const setKubeconfigFromEvent = (event) => setKubeconfig(event.target.value);
    const handleSubmit = (event) => {
        event.preventDefault();
        event.stopPropagation();
        updateAuth({ kind: "kubeconfig", params: { kubeconfig }});
        history.replace(from);
    };
    return (
        <>
            <Row><Col><h1 className="border-bottom pb-2 mb-4">Authenticate with Kubernetes</h1></Col></Row>
            <Row>
                <Col>
                    <p>Provide a <code>kubeconfig</code> file to use when connecting to Kubernetes.</p>
                </Col>
            </Row>
            <Row className="justify-content-center">
                <Col lg={10} xl={8}>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group as={Row} controlId="kubeconfig">
                            <Form.Label column md={2}>Kubeconfig</Form.Label>
                            <Col>
                                <Form.Control
                                    as="textarea"
                                    rows="10"
                                    required
                                    value={kubeconfig}
                                    onChange={setKubeconfigFromEvent}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row}>
                            <Col sm={{ span: 10, offset: 2 }}>
                                <Button type="submit">Save</Button>
                            </Col>
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
        </>
    );
});


export default Auth;
