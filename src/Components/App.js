import '../assets/scss/App.scss';

import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import Container from 'react-bootstrap/Container';

import Nav from './Navigation';
import Notifications from './Notifications';
import Scanners from './Scanners';
import ImageScan from './ImageScan';
import NamespaceScan from './NamespaceScan';

import { observer } from 'mobx-react';


const App = observer(({ notifications, scanners, imageScan, namespaceScan }) => {
    return (
        <Router>
            <Nav />
            <Notifications store={notifications} />
            <Container fluid>
                <Switch>
                    <Route exact path="/scanners"><Scanners scanners={scanners} /></Route>
                    <Route exact path="/scans/image"><ImageScan imageScan={imageScan} /></Route>
                    <Route exact path="/scans/namespace"><NamespaceScan namespaceScan={namespaceScan} /></Route>
                    <Route path="/"><Redirect to="/scans/namespace" /></Route>
                </Switch>
            </Container>
        </Router>
    );
});


export default App;
