import React from 'react';

import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';

import { LinkContainer } from 'react-router-bootstrap';


const Navigation = () => (
    <Navbar variant="light" bg="light" className="mb-3">
        <Container fluid>
            <Navbar.Brand>Soter Security Dashboard</Navbar.Brand>
            <Navbar.Toggle aria-controls="nav-main" />
            <Navbar.Collapse id="nav-main">
                <Nav className="mr-auto">
                    <LinkContainer to="/scanners">
                        <Nav.Link>Scanners</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to="/scans/image">
                        <Nav.Link>Image scan</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to="/scans/namespace">
                        <Nav.Link>Namespace scan</Nav.Link>
                    </LinkContainer>
                </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
);


export default Navigation;
