import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

import { FontAwesomeIcon as Icon } from '@fortawesome/react-fontawesome';
import {
    faCheckCircle,
    faCog,
    faTimesCircle,
    faSync
} from '@fortawesome/free-solid-svg-icons';

import { autorun, entries } from 'mobx';
import { observer } from 'mobx-react';


const StatusProperties = observer(({ name, properties, show, onHide }) => {
    // Order the properties by key
    const sortedProps = entries(properties).sort(
        (x, y) => (x[0] < y[0] ? -1 : (x[0] > y[0] ? 1 : 0))
    );
    return (
        <Modal show={show} onHide={onHide} size="lg">
            <Modal.Header closeButton>
                <Modal.Title>Scanner properties: <code>{name}</code></Modal.Title>
            </Modal.Header>
            <Table className="modal-body" striped size="md">
                <tbody>
                    {sortedProps.map(([key, value]) =>
                        <tr key={key}>
                            <td className="text-right"><code>{key}</code></td>
                            <td>{value || '-'}</td>
                        </tr>
                    )}
                </tbody>
            </Table>
            <Modal.Footer>
                <Button variant="secondary" onClick={onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
});


const ScannerRow = observer(({ name, status }) => {
    const [modalVisible, setModalVisible] = React.useState(false);
    const showModal = () => setModalVisible(true);
    const hideModal = () => setModalVisible(false);
    const numProps = entries(status.properties).length;
    return (
        <tr>
            <td><code>{name}</code></td>
            <td>{status.kind}</td>
            <td>{status.vendor}</td>
            <td>{status.version}</td>
            <td className={`text-center ${status.available ? "text-success" : "text-danger"}`}>
                <Icon icon={status.available ? faCheckCircle : faTimesCircle} size="lg" />
            </td>
            <td>{status.message || '-'}</td>
            <td>
                <Button variant="link" onClick={showModal} disabled={numProps < 1}>
                    {numProps} properties
                </Button>
                <StatusProperties
                    name={name}
                    properties={status.properties}
                    show={modalVisible}
                    onHide={hideModal}
                />
            </td>
        </tr>
    );
});


const ScannersTable = observer(({ scanners }) => {
    return (
        <>
            <Row className="mb-2">
                <Col className="text-right">
                    <Button onClick={scanners.load} disabled={scanners.loading}>
                        <Icon icon={faSync} spin={scanners.loading} className="mr-2" />
                        Refresh
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Table striped hover bordered size="md">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Kind</th>
                                <th>Vendor</th>
                                <th>Version</th>
                                <th>Available?</th>
                                <th>Message</th>
                                <th>Properties</th>
                            </tr>
                        </thead>
                        <tbody>
                            {scanners.data.size > 0 ?
                                Array.from(scanners.data.entries()).map(([name, status]) =>
                                    <ScannerRow key={name} name={name} status={status} />
                                ) : (
                                   <tr><td colSpan="7">No scanners available.</td></tr>
                                )
                            }
                        </tbody>
                        <caption className="px-3">{scanners.data.size} scanners</caption>
                    </Table>
                </Col>
            </Row>
        </>
    );
});


const Scanners = observer(({ scanners }) => {
    // The first time the component is mounted, load the scanners
    React.useEffect(() => autorun(scanners.initialise), [scanners]);
    return (
        <>
            <Row><Col><h1 className="border-bottom pb-2 mb-4">Scanners</h1></Col></Row>
            {scanners.data !== undefined ?
                <ScannersTable scanners={scanners} /> :
                <Row>
                    <Col>
                        <div className="d-flex justify-content-center align-items-center py-5">
                            <Icon icon={faCog} size="2x" spin />
                            <div className="d-inline-block ml-2">Loading scanners...</div>
                        </div>
                    </Col>
                </Row>
            }
        </>
    );
});


export default Scanners;
