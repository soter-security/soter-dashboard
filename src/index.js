import React from 'react';
import ReactDOM from 'react-dom';

import { addMiddleware } from 'mobx-state-tree';

import makeInspectable from 'mobx-devtools-mst';

import Client, { mstMiddleware } from './jsonrpc';
import RootStore from './store/root';
import App from './Components/App';


// Create a JSON-RPC client
const client = Client.create('ws://127.0.0.1:5000');

// Hook up the store to the client and configure the middleware to report errors
const store = RootStore.create({}, { client });
addMiddleware(
    store,
    mstMiddleware(
        // This handles RPC errors
        (err) => store.notifications.danger(err.message, err.data),
        // This handles connection errors
        (err) => store.notifications.danger(err.title, err.message)
    )
);
makeInspectable(store);

// Set up React
ReactDOM.render(
    <React.StrictMode>
        <App
            notifications={store.notifications}
            scanners={store.scanners}
            imageScan={store.imageScan}
            namespaceScan={store.namespaceScan}
        />
    </React.StrictMode>,
    document.getElementById('root')
);
