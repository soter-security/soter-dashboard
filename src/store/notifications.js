import { types, getParentOfType } from "mobx-state-tree";
import { v4 as uuidv4 } from 'uuid';


export const Levels = {
    INFO: 'info',
    SUCCESS: 'success',
    WARNING: 'warning',
    DANGER: 'danger'
};


const Notification = types
    .model('Notification', {
        id: types.optional(types.identifier, uuidv4),
        title: types.string,
        message: types.string,
        level: types.enumeration('Level', [
            Levels.INFO,
            Levels.SUCCESS,
            Levels.WARNING,
            Levels.DANGER
        ]),
        // The duration of time to show the notification for
        // If not given, the notification stays until dismissed
        duration: types.maybe(types.integer)
    })
    .actions(self => ({
        dismiss: () => getParentOfType(self, NotificationStore).remove(self.id)
    }));


const NotificationStore = types
    .model('NotificationStore', {
        notifications: types.map(Notification)
    })
    .actions(self => {
        const notify = (level, title, message, duration) => {
            const notification = Notification.create({ title, message, level, duration });
            self.notifications.set(notification.id, notification);
        };

        return {
            notify,
            info: notify.bind(null, Levels.INFO),
            success: notify.bind(null, Levels.SUCCESS),
            warning: notify.bind(null, Levels.WARNING),
            danger: notify.bind(null, Levels.DANGER),
            remove: (id) => self.notifications.delete(id),
            clear: () => self.notifications.clear()
        };
    });


export default NotificationStore;
