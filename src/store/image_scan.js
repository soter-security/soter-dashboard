import { types, flow, getRoot, getEnv } from 'mobx-state-tree';


const ImageScan = types
    .model('ImageScan', {
        image: types.string,
        digest: types.string,
        issues: types.array(types.frozen()),
        createdAt: types.optional(types.Date, () => new Date())
    });


const ImageScanStore = types
    .model('ImageScanStore', {
        // Image scans indexed by digest
        scanResult: types.maybe(ImageScan),
        scanInProgress: false
    })
    .actions(self => ({
        executeScan: flow(function*(image) {
            self.scanInProgress = true;
            try {
                self.scanResult = ImageScan.create(
                    yield getEnv(self).client("image.scan", { image })
                );
                getRoot(self).notifications.success(
                    'Image scan complete',
                    'Image scan completed successfully.',
                    3000
                );
            }
            catch(e) {
                self.scanResult = undefined;
            }
            finally {
                self.scanInProgress = false;
            }
        }),
        setScanResult: (scanResult) => {
            self.scanResult = ImageScan.create(scanResult);
            self.scanInProgress = false;
        }
    }));


export default ImageScanStore;
