import { types, flow, getEnv, getRoot } from "mobx-state-tree";


const Scanner = types
    .model('Scanner', {
        kind: types.string,
        vendor: types.string,
        version: types.string,
        available: types.boolean,
        message: types.optional(types.string, ''),
        properties: types.optional(types.map(types.frozen()), {}, [undefined, null])
    });


const ScannerStore = types
    .model('ScannerStore', {
        loading: false,
        data: types.maybe(types.map(Scanner))
    })
    .actions(self => ({
        initialise: flow(function*() {
            if( self.data === undefined ) yield self.load();
        }),
        load: flow(function*() {
            self.loading = true;
            try {
                self.data = yield getEnv(self).client("info.scanners");
                getRoot(self).notifications.success(
                    'Scanners loaded',
                    'Scanners loaded successfully.',
                    3000
                );
            }
            catch(e) {
                console.log(e);
                self.data = {};
            }
            finally {
                self.loading = false;
            }
        })
    }));


export default ScannerStore;
