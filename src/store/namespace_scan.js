import { types, flow, getRoot, getEnv, applySnapshot, onSnapshot } from "mobx-state-tree";
import { v4 as uuidv4 } from 'uuid';


const Profile = types
    .model('Profile', {
        id: types.optional(types.identifier, uuidv4),
        name: types.string,
        kind: types.string,
        params: types.frozen()
    });


const Resource = types
    .model('Resource', {
        namespace: types.string,
        kind: types.string,
        name: types.string
    });


const NamespaceScan = types
    .model('NamespaceScan', {
        resources: types.array(Resource),
        issues: types.array(types.frozen()),
        createdAt: types.optional(types.Date, () => new Date())
    });


const NamespaceScanStore = types
    .model('NamespaceScanStore', {
        // The available auth kinds, indexed by kind
        authKinds: types.map(types.frozen()),
        // Allow for multiple authentication profiles for connecting to different clusters
        profiles: types.array(Profile),
        activeProfile: types.safeReference(Profile),
        // The clusters for the active profile
        clusters: types.maybe(types.array(types.string)),
        activeCluster: types.maybe(types.string),
        // The namespaces for the active cluster
        namespaces: types.maybe(types.array(types.string)),
        activeNamespace: types.maybe(types.string),
        // Information about the current scan
        scanInProgress: false,
        scanResult: types.maybe(NamespaceScan)
    })
    .views(self => ({
        getProfileById: (id) => {
            const index = self.profiles.findIndex(p => p.id === id);
            return self.profiles[index];
        }
    }))
    .actions(self => ({
        afterAttach: flow(function*() {
            // Load the auth kinds
            try {
                self.authKinds = yield getEnv(self).client("namespace.auth_kinds");
            }
            catch(e) {
                self.authKinds = {};
                // If the fetching of the auth kinds fails, don't do any of the profile management
                return;
            }
            // Restore any previous profiles from local storage
            // If any of the profiles are for kinds that are no longer available, discard them
            const savedProfilesJSON = window.localStorage.getItem('profiles');
            const savedProfiles = savedProfilesJSON ? JSON.parse(savedProfilesJSON) : [];
            self.profiles = savedProfiles.filter(p => self.authKinds.has(p.kind));
            // Restore the active profile from local storage
            const activeProfileId = window.localStorage.getItem('activeProfileId');
            const activeProfileExists = self.profiles.some(p => p.id === activeProfileId);
            yield self.setActiveProfile(activeProfileExists ? activeProfileId : undefined);
            // Persist any changes to the profiles in localStorage
            onSnapshot(self, (snapshot) => {
                window.localStorage.setItem('profiles', JSON.stringify(snapshot.profiles));
                if( snapshot.activeProfile ) {
                    window.localStorage.setItem('activeProfileId', snapshot.activeProfile);
                }
                else {
                    window.localStorage.removeItem('activeProfileId');
                }
            });
        }),
        addProfile: (profileData) => {
            const profile = Profile.create(profileData);
            self.profiles.push(profile);
            return profile;
        },
        updateProfile: (id, updates) => {
            const profile = self.getProfileById(id);
            Object.entries(updates).forEach(([name, value]) => {
                profile[name] = value;
            });
            return profile;
        },
        removeProfile: function(id) {
            const isActiveProfile = self.activeProfile?.id === id;
            const index = self.profiles.findIndex(p => p.id === id);
            self.profiles.splice(index, 1);
            // If removing the active profile, we need to do a reset
            if( isActiveProfile ) applySnapshot(self, {
                authKinds: self.authKinds,
                profiles: self.profiles
            });
        },
        setActiveProfile: flow(function*(activeProfile) {
            // Changing the profile should reset everything except the profiles
            applySnapshot(self, {
                authKinds: self.authKinds,
                profiles: self.profiles,
                activeProfile
            });
            // If the profile was reset to nothing, we are done
            if( !self.activeProfile ) return;
            // Otherwise, load the clusters for the new profile
            try {
                self.clusters = yield getEnv(self).client("namespace.clusters", {
                    auth: {
                        kind: self.activeProfile.kind,
                        ...self.activeProfile.params
                    }
                });
                getRoot(self).notifications.success(
                    'Clusters loaded',
                    'Clusters loaded successfully.',
                    3000
                );
            }
            catch(e) {
                self.clusters = [];
            }
            // If there is exactly one cluster, select it
            if( self.clusters.length === 1 ) {
                yield self.setActiveCluster(self.clusters[0]);
            }
        }),
        setActiveCluster: flow(function*(cluster) {
            // Changing the active cluster should reset the namespaces
            self.activeCluster = cluster;
            self.namespaces = undefined;
            self.activeNamespace = undefined;
            // If the active cluster was being unset, we are done
            if( !cluster ) return;
            // Otherwise, fetch the namespaces for the new cluster
            try {
                self.namespaces = yield getEnv(self).client("namespace.namespaces", {
                    auth: {
                        kind: self.activeProfile.kind,
                        ...self.activeProfile.params
                    },
                    cluster
                });
                getRoot(self).notifications.success(
                    'Namespaces loaded',
                    'Namespaces loaded successfully.',
                    3000
                );
            }
            catch(e) {
                self.namespaces = [];
            }
            // If there is exactly one namespace, select it
            if( self.namespaces.length === 1 )
                self.setActiveNamespace(self.namespaces[0]);
        }),
        setActiveNamespace: (namespace) => {
            self.activeNamespace = namespace;
        },
        executeScan: flow(function*() {
            self.scanInProgress = true;
            try {
                self.scanResult = NamespaceScan.create(
                    yield getEnv(self).client("namespace.scan", {
                        auth: {
                            kind: self.activeProfile.kind,
                            ...self.activeProfile.params
                        },
                        cluster: self.activeCluster,
                        namespace: self.activeNamespace
                    })
                );
                getRoot(self).notifications.success(
                    'Report generated',
                    'Report generated successfully.',
                    3000
                );
            }
            catch(e) {
                self.scanResult = undefined;
            }
            finally {
                self.scanInProgress = false;
            }
        })
    }));


export default NamespaceScanStore;
