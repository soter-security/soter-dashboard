import { types } from 'mobx-state-tree';

import NotificationStore from './notifications';
import ScannerStore from './scanners';
import ImageScanStore from './image_scan'
import NamespaceScanStore from './namespace_scan';


const RootStore = types
    .model('RootStore', {
        notifications: types.optional(NotificationStore, () => NotificationStore.create()),
        scanners: types.optional(ScannerStore, () => ScannerStore.create()),
        imageScan: types.optional(ImageScanStore, () => ImageScanStore.create()),
        namespaceScan: types.optional(NamespaceScanStore, () => NamespaceScanStore.create())
    });


export default RootStore;
